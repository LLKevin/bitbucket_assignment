﻿/*
 DesignForm.cs
 Purpose: This is to create and interact with the controls on the form.
           
 Revision History:
    Kevin Lucas 2018.09.30: Created
 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KLucasAssignment1
{
     public partial class DesignForm : Form
    {
      
        public DesignForm()
        {
            InitializeComponent();
          
        }
        
        //saves the created picturebox to an array
        public PictureBox[,] gameBoard;

        /// <summary>
        /// This will create an instance of the generateTile and
        /// add pictureboxes to the form based on propeties set in the GenerateTile Class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerateGrid_Click(object sender, EventArgs e)
        {
            int rows;
            int columns;

            bool validRowNumber = false;
            bool validColNumber = false;

            validRowNumber = int.TryParse(txtRows.Text, out rows);
            validColNumber = int.TryParse(txtColumns.Text, out columns);

            if (validRowNumber == false || validColNumber == false)
            {
                MessageBox.Show("Please enter valid value for rows and columns (both must be intergers)");
            }
            else
            {
                //get the board size
                gameBoard = new PictureBox[rows, columns];

                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                       GenerateTile tile = new GenerateTile(j, i, this);
                       gameBoard[i, j] = tile;
                       this.Controls.Add(tile);
                    }
                }
            }
        }

        /// <summary>
        ///  closes the designform.
        /// </summary>
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// This is a click event for the saving control. 
        /// it will create an new instance of the class to call the save method. 
        /// which will be handled by the GenerateTile class.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
         GenerateTile saveGrid = new GenerateTile(this);
         saveGrid.SaveFile(grid: gameBoard);
  
        }
    }
}


