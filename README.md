Simple Map editor 
------------------------------------
You can use this application by download or cloning the repo.
Then you will need to execute the code using visual studio as there isn't an executable file yet.

License
--------------------------------------------
This Project is licensed under the MIT License - see the License.md file for details
I choose this License because I don't mine anyone using the project with little restriction. 
I just don't want to be responsible or liable for any damages the software may cause. 
I also don't want to offer any warranties.

add something to the readme