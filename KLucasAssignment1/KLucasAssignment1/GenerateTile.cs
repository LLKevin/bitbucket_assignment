﻿/* 
 GenerateTile.cs

  The purpose of this class is to handles all interaction of the system. 
  this will set all grid tile properties and calculate the location of each grid tile. 
  using the selected button tool is assigned to a grid tile when clicked as well as a integer the image represents.
  lastly, this will save the grid as an integer.

  Revision History:
    Kevin Lucas, 2018.09.30: Created
 
 */
using KLucasAssignment1.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace KLucasAssignment1
{
    /// <summary>
    /// contains all the materials
    /// that will be used in the buttons as an enum.
    /// </summary>
    public enum materials
    {
        NONE,
        WALL,
        REDDOOR,
        BLUEDOOR,
        GREENDOOR,
        YELLOWDOOR,
        REDBOX,
        BLUEBOX,
        GREENBOX,
        YELLOWBOX
    }
    class GenerateTile : PictureBox
    {
        const int StaringLocationX = 220;
        const int StartingLocationY = 120;
        const int TileWidth = 50;
        const int TileHeight = 50;
        const int gap = 0;

        private int x;
        private int y;

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        //contains the form where the form controls are located.
        public Form designFormObject;
    
        /// <summary>
        ///  This Constructor will add properties to get grid tile 
        ///  and will position each tile in a grid layout based on the parameters sent
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="form"></param>
        public GenerateTile(int x, int y, object form)
        {

            this.Left = StaringLocationX + x * (TileWidth + gap);
            this.Top = StartingLocationY + y * (TileHeight + gap);
            this.Width = TileWidth;
            this.Height = TileHeight;
            this.BorderStyle = BorderStyle.Fixed3D;
            this.Tag = 0;

            designFormObject = (Form)form;
           
            this.Click += TileHandler;

            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// constructor to get the form object -- this is to be used only when saving.
        /// </summary>
        public GenerateTile(object form)
        {
            designFormObject = (Form)form;
        }

        /// <summary>
        /// This method is to be a void click handler for when a grid is clicked. 
        /// This method will located the selected tool image and  load it to the click grid tile from resources. 
        /// it will allow assign a int value based on the image assign to the grid tile.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TileHandler(object sender, EventArgs e)
        {
            GenerateTile tile = sender as GenerateTile;
            string controlName = designFormObject.ActiveControl.Text;
            //clear the space in the name to allow the manger to find the control from resource.
            controlName = Regex.Replace(controlName, @"\s+", "");

            tile.BackgroundImageLayout = ImageLayout.Stretch;
            //get image
            tile.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject(controlName);
            //get image number
            tile.Tag = GetImageNumber(); //image number.....
        }

        /// <summary>
        /// This method is to get the imageNumber from the enum 
        /// and return it to the calling method.
        /// </summary>
        /// <returns></returns>
        public int GetImageNumber()
        {         
           string selection = designFormObject.ActiveControl.Text;
            selection = Regex.Replace(selection, @"\s+", "");
            int selectionInt = 0;

            switch (selection.ToUpper())
            {
                case "NONE":
                    selectionInt = (int)materials.NONE;
                    break;
                case "WALL":
                    selectionInt = (int)materials.WALL;
                    break;
                case "REDDOOR":
                    selectionInt = (int)materials.REDDOOR;
                    break;
                case "BLUEDOOR":
                    selectionInt = (int)materials.BLUEDOOR;
                    break;
                case "GREENDOOR":
                    selectionInt = (int)materials.GREENDOOR;
                    break;
                case "YELLOWDOOR":
                    selectionInt = (int)materials.YELLOWDOOR;
                    break;
                case "REDBOX":
                    selectionInt = (int)materials.REDBOX;
                    break;
                case "BLUEBOX":
                    selectionInt = (int)materials.BLUEBOX;
                    break;
                case "GREENBOX":
                    selectionInt = (int)materials.GREENBOX;
                    break;
                case "YELLOWBOX":
                    selectionInt = (int)materials.YELLOWBOX;
                    break;
                default: selectionInt = (int)materials.NONE;
                    break;
            }
            return selectionInt;
        }

        /// <summary>
        ///The purpose of this method is to save the grid to the file as integer 
        ///values in the follwing format: Number of Rows, Number of Columns
        ///for each Cell: Cell's Row, Cell's Column, Cell's Content
        /// </summary>
        /// <param name="grid"></param>
        public void SaveFile(PictureBox[,] grid)
        {
            string filePath = "";
            SaveFileDialog savedMap = new SaveFileDialog();
            savedMap.FileName = "KLucasAssignmentLevel1.qgame";
            savedMap.DefaultExt = "qgame";         
            savedMap.AddExtension = true;
           
            if(savedMap.ShowDialog() == DialogResult.OK)
            {
                filePath = savedMap.FileName;
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    //give me the total rows and total cols
                     writer.WriteLine(grid.GetLength(0));
                     writer.WriteLine(grid.GetLength(1));
                
                    for(int i =0; i < grid.GetLength(0);i++)
                    {
                        for(int j =0; j < grid.GetLength(1); j++)
                        {
                           writer.WriteLine(i);
                           writer.WriteLine(j);
                           writer.WriteLine(grid[i,j].Tag);
                        }
                    }
                    writer.Close();
                    MessageBox.Show("Save File Successfully");
                }
            }
        }
    }
}