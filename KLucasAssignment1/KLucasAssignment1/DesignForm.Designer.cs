﻿namespace KLucasAssignment1
{
    partial class DesignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignForm));
            this.txtRows = new System.Windows.Forms.TextBox();
            this.txtColumns = new System.Windows.Forms.TextBox();
            this.lblRows = new System.Windows.Forms.Label();
            this.btnGenerateGrid = new System.Windows.Forms.Button();
            this.lblColumns = new System.Windows.Forms.Label();
            this.toolBoxImgList = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGreenBox = new System.Windows.Forms.Button();
            this.btnYellowDoor = new System.Windows.Forms.Button();
            this.btnWall = new System.Windows.Forms.Button();
            this.btnBlueBox = new System.Windows.Forms.Button();
            this.btnRedBox = new System.Windows.Forms.Button();
            this.btnRedDoor = new System.Windows.Forms.Button();
            this.btnYellowBox = new System.Windows.Forms.Button();
            this.btnNone = new System.Windows.Forms.Button();
            this.btnGreenDoor = new System.Windows.Forms.Button();
            this.btnBlueDoor = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRows
            // 
            this.txtRows.Location = new System.Drawing.Point(51, 15);
            this.txtRows.Name = "txtRows";
            this.txtRows.Size = new System.Drawing.Size(100, 20);
            this.txtRows.TabIndex = 0;
            // 
            // txtColumns
            // 
            this.txtColumns.Location = new System.Drawing.Point(219, 15);
            this.txtColumns.Name = "txtColumns";
            this.txtColumns.Size = new System.Drawing.Size(100, 20);
            this.txtColumns.TabIndex = 1;
            // 
            // lblRows
            // 
            this.lblRows.AutoSize = true;
            this.lblRows.Location = new System.Drawing.Point(10, 18);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(34, 13);
            this.lblRows.TabIndex = 2;
            this.lblRows.Text = "Rows";
            // 
            // btnGenerateGrid
            // 
            this.btnGenerateGrid.Location = new System.Drawing.Point(343, 15);
            this.btnGenerateGrid.Name = "btnGenerateGrid";
            this.btnGenerateGrid.Size = new System.Drawing.Size(75, 22);
            this.btnGenerateGrid.TabIndex = 3;
            this.btnGenerateGrid.Text = "Generate";
            this.btnGenerateGrid.UseVisualStyleBackColor = true;
            this.btnGenerateGrid.Click += new System.EventHandler(this.btnGenerateGrid_Click);
            // 
            // lblColumns
            // 
            this.lblColumns.AutoSize = true;
            this.lblColumns.Location = new System.Drawing.Point(166, 18);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(47, 13);
            this.lblColumns.TabIndex = 4;
            this.lblColumns.Text = "Columns";
            // 
            // toolBoxImgList
            // 
            this.toolBoxImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("toolBoxImgList.ImageStream")));
            this.toolBoxImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.toolBoxImgList.Images.SetKeyName(0, "GreyBox.jpg");
            this.toolBoxImgList.Images.SetKeyName(1, "Wall.jpg");
            this.toolBoxImgList.Images.SetKeyName(2, "RedDoor.png");
            this.toolBoxImgList.Images.SetKeyName(3, "BlueDoor.png");
            this.toolBoxImgList.Images.SetKeyName(4, "GreenDoor.png");
            this.toolBoxImgList.Images.SetKeyName(5, "YellowDoor.png");
            this.toolBoxImgList.Images.SetKeyName(6, "RedBox.png");
            this.toolBoxImgList.Images.SetKeyName(7, "GreenBox.png");
            this.toolBoxImgList.Images.SetKeyName(8, "BlueBox.png");
            this.toolBoxImgList.Images.SetKeyName(9, "YellowBox.png");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "Toolbox";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(891, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(891, 24);
            this.menuStrip2.TabIndex = 19;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // btnGreenBox
            // 
            this.btnGreenBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGreenBox.ImageIndex = 7;
            this.btnGreenBox.ImageList = this.toolBoxImgList;
            this.btnGreenBox.Location = new System.Drawing.Point(13, 405);
            this.btnGreenBox.Name = "btnGreenBox";
            this.btnGreenBox.Size = new System.Drawing.Size(135, 47);
            this.btnGreenBox.TabIndex = 16;
            this.btnGreenBox.Tag = "7";
            this.btnGreenBox.Text = "Green Box";
            this.btnGreenBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGreenBox.UseVisualStyleBackColor = true;
            // 
            // btnYellowDoor
            // 
            this.btnYellowDoor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnYellowDoor.ImageIndex = 5;
            this.btnYellowDoor.ImageList = this.toolBoxImgList;
            this.btnYellowDoor.Location = new System.Drawing.Point(13, 297);
            this.btnYellowDoor.Name = "btnYellowDoor";
            this.btnYellowDoor.Size = new System.Drawing.Size(135, 47);
            this.btnYellowDoor.TabIndex = 12;
            this.btnYellowDoor.Tag = "5";
            this.btnYellowDoor.Text = "Yellow Door";
            this.btnYellowDoor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnYellowDoor.UseVisualStyleBackColor = true;
            // 
            // btnWall
            // 
            this.btnWall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWall.ImageIndex = 1;
            this.btnWall.ImageList = this.toolBoxImgList;
            this.btnWall.Location = new System.Drawing.Point(12, 83);
            this.btnWall.Name = "btnWall";
            this.btnWall.Size = new System.Drawing.Size(135, 47);
            this.btnWall.TabIndex = 11;
            this.btnWall.Tag = "1";
            this.btnWall.Text = "Wall";
            this.btnWall.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnWall.UseVisualStyleBackColor = true;
            // 
            // btnBlueBox
            // 
            this.btnBlueBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBlueBox.ImageIndex = 8;
            this.btnBlueBox.ImageList = this.toolBoxImgList;
            this.btnBlueBox.Location = new System.Drawing.Point(13, 459);
            this.btnBlueBox.Name = "btnBlueBox";
            this.btnBlueBox.Size = new System.Drawing.Size(135, 47);
            this.btnBlueBox.TabIndex = 15;
            this.btnBlueBox.Tag = "8";
            this.btnBlueBox.Text = "Blue Box";
            this.btnBlueBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBlueBox.UseVisualStyleBackColor = true;
            // 
            // btnRedBox
            // 
            this.btnRedBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRedBox.ImageIndex = 6;
            this.btnRedBox.ImageList = this.toolBoxImgList;
            this.btnRedBox.Location = new System.Drawing.Point(13, 351);
            this.btnRedBox.Name = "btnRedBox";
            this.btnRedBox.Size = new System.Drawing.Size(135, 47);
            this.btnRedBox.TabIndex = 13;
            this.btnRedBox.Tag = "6";
            this.btnRedBox.Text = "Red Box";
            this.btnRedBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRedBox.UseVisualStyleBackColor = true;
            // 
            // btnRedDoor
            // 
            this.btnRedDoor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRedDoor.ImageIndex = 2;
            this.btnRedDoor.ImageList = this.toolBoxImgList;
            this.btnRedDoor.Location = new System.Drawing.Point(12, 137);
            this.btnRedDoor.Name = "btnRedDoor";
            this.btnRedDoor.Size = new System.Drawing.Size(135, 47);
            this.btnRedDoor.TabIndex = 10;
            this.btnRedDoor.Tag = "2";
            this.btnRedDoor.Text = "Red Door";
            this.btnRedDoor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRedDoor.UseVisualStyleBackColor = true;
            // 
            // btnYellowBox
            // 
            this.btnYellowBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnYellowBox.ImageIndex = 9;
            this.btnYellowBox.ImageList = this.toolBoxImgList;
            this.btnYellowBox.Location = new System.Drawing.Point(14, 512);
            this.btnYellowBox.Name = "btnYellowBox";
            this.btnYellowBox.Size = new System.Drawing.Size(135, 47);
            this.btnYellowBox.TabIndex = 14;
            this.btnYellowBox.Tag = "9";
            this.btnYellowBox.Text = "Yellow Box";
            this.btnYellowBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnYellowBox.UseVisualStyleBackColor = true;
            // 
            // btnNone
            // 
            this.btnNone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNone.ImageIndex = 0;
            this.btnNone.ImageList = this.toolBoxImgList;
            this.btnNone.Location = new System.Drawing.Point(12, 29);
            this.btnNone.Name = "btnNone";
            this.btnNone.Size = new System.Drawing.Size(135, 47);
            this.btnNone.TabIndex = 7;
            this.btnNone.Tag = "0";
            this.btnNone.Text = "None";
            this.btnNone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNone.UseVisualStyleBackColor = true;
            // 
            // btnGreenDoor
            // 
            this.btnGreenDoor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGreenDoor.ImageIndex = 4;
            this.btnGreenDoor.ImageList = this.toolBoxImgList;
            this.btnGreenDoor.Location = new System.Drawing.Point(13, 244);
            this.btnGreenDoor.Name = "btnGreenDoor";
            this.btnGreenDoor.Size = new System.Drawing.Size(135, 47);
            this.btnGreenDoor.TabIndex = 9;
            this.btnGreenDoor.Tag = "4";
            this.btnGreenDoor.Text = "Green Door";
            this.btnGreenDoor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGreenDoor.UseVisualStyleBackColor = true;
            // 
            // btnBlueDoor
            // 
            this.btnBlueDoor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBlueDoor.ImageIndex = 3;
            this.btnBlueDoor.ImageList = this.toolBoxImgList;
            this.btnBlueDoor.Location = new System.Drawing.Point(13, 191);
            this.btnBlueDoor.Name = "btnBlueDoor";
            this.btnBlueDoor.Size = new System.Drawing.Size(135, 47);
            this.btnBlueDoor.TabIndex = 8;
            this.btnBlueDoor.Tag = "3";
            this.btnBlueDoor.Text = "Blue Door";
            this.btnBlueDoor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBlueDoor.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnBlueBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnBlueDoor);
            this.panel1.Controls.Add(this.btnGreenBox);
            this.panel1.Controls.Add(this.btnGreenDoor);
            this.panel1.Controls.Add(this.btnYellowDoor);
            this.panel1.Controls.Add(this.btnNone);
            this.panel1.Controls.Add(this.btnWall);
            this.panel1.Controls.Add(this.btnYellowBox);
            this.panel1.Controls.Add(this.btnRedDoor);
            this.panel1.Controls.Add(this.btnRedBox);
            this.panel1.Location = new System.Drawing.Point(2, 71);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 573);
            this.panel1.TabIndex = 20;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.txtColumns);
            this.panel2.Controls.Add(this.txtRows);
            this.panel2.Controls.Add(this.lblColumns);
            this.panel2.Controls.Add(this.lblRows);
            this.panel2.Controls.Add(this.btnGenerateGrid);
            this.panel2.Location = new System.Drawing.Point(1, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(890, 46);
            this.panel2.TabIndex = 21;
            // 
            // DesignForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(890, 654);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip2;
            this.Name = "DesignForm";
            this.Text = "DesignForm";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRows;
        private System.Windows.Forms.TextBox txtColumns;
        private System.Windows.Forms.Label lblRows;
        private System.Windows.Forms.Button btnGenerateGrid;
        private System.Windows.Forms.Label lblColumns;
        private System.Windows.Forms.ImageList toolBoxImgList;
        private System.Windows.Forms.Button btnNone;
        private System.Windows.Forms.Button btnBlueDoor;
        private System.Windows.Forms.Button btnGreenDoor;
        private System.Windows.Forms.Button btnRedDoor;
        private System.Windows.Forms.Button btnWall;
        private System.Windows.Forms.Button btnYellowDoor;
        private System.Windows.Forms.Button btnGreenBox;
        private System.Windows.Forms.Button btnBlueBox;
        private System.Windows.Forms.Button btnRedBox;
        private System.Windows.Forms.Button btnYellowBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}